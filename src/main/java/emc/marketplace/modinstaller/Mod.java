package emc.marketplace.modinstaller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import emc.marketplace.main.Main;
import lombok.Data;
import me.deftware.client.framework.FrameworkConstants;
import me.deftware.client.framework.main.EMCMod;
import me.deftware.client.framework.main.bootstrap.Bootstrap;
import me.deftware.client.framework.utils.HashUtils;
import me.deftware.client.framework.path.OSUtils;
import me.deftware.client.framework.wrappers.IMinecraft;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public @Data class Mod {

    /*
        Serialized data
     */

    public @Expose @SerializedName("beta") boolean beta;

    public @Expose @SerializedName("id") String id;

    public @Expose @SerializedName("name") String name;
    public @Expose @SerializedName("author") String author;
    public @Expose @SerializedName("version") String version;
    public @Expose @SerializedName("sha1") String sha1;
    public @Expose @SerializedName("link") String link;
    public @Expose @SerializedName("summary") String summary;

    public @Expose @SerializedName("depends") ArrayList<String> depends;
    public @Expose @SerializedName("description") ArrayList<String> description;

    public @Expose @SerializedName("fabric") JsonObject fabric;

    /*
        Mod data
     */

    public @Exclude boolean updated = false;

    public @Exclude File modFile = null, deleted = null, update = null;
    public @Exclude EMCMod mod = null;

    /*
        Fabric specific data
     */

    public @Exclude String emcPath, fabricModVersion;
    public @Exclude File fabricMod, fabricModUpdate, fabricModDelete;
    public @Exclude boolean updateFabricMod = false;

    void init() {
        try {
            String EMCDir = OSUtils.getMCDir() + "libraries" + File.separator + "EMC" + File.separator + IMinecraft.getMinecraftVersion() + File.separator;
            modFile = new File(EMCDir + id + ".jar");
            deleted = new File(EMCDir + id + ".jar.delete");
            update = new File(EMCDir + id + ".jar.update");
            if (modFile.exists() && !API.getVerifiedMods().contains(id)) {
                API.getVerifiedMods().add(id);
                verifyFile();
            }
            if (fabric != null) {
                try {
                    JsonObject versions = fabric.get("versions").getAsJsonObject();
                    if (versions.has(IMinecraft.getMinecraftVersion())) {
                        JsonObject data = versions.get(IMinecraft.getMinecraftVersion()).getAsJsonObject();
                        emcPath = System.getProperty("EMCDir", OSUtils.getMCDir()) + File.separator;

                        String fileName = fabric.get("fileName").getAsString().replace("%mc%", IMinecraft.getMinecraftVersion());

                        link = data.get("url").getAsString();
                        fabricMod = new File(emcPath + fileName + ".jar");
                        fabricModUpdate = new File(emcPath + fileName + ".jar.update");
                        fabricModDelete = new File(emcPath + fileName + ".jar.delete");
                        fabricModVersion = data.get("version").getAsString();

                        if (!Main.INSTANCE.getSettings().getString(id + "_fabric_version", fabricModVersion).equals(fabricModVersion)) {
                            updateFabricMod = true;
                            Main.INSTANCE.getSettings().saveString(id + "_fabric_version", data.get("version").getAsString());
                            Main.INSTANCE.getSettings().saveConfig();
                            Main.logger.info("Updating fabric mod " + id);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                verifyFile();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isInstalled() {
        return (modFile != null && deleted != null && (modFile.exists() && !deleted.exists())) || (fabricMod != null && fabricModDelete != null && fabricMod.exists() && !fabricModDelete.exists());
    }

    private void verifyFile() {
        new Thread(() -> {
            try {
                if (fabric != null) {
                    if (updateFabricMod) {
                        if (!fabricModUpdate.exists()) {
                            Web.downloadMod(link, fabricModUpdate.getAbsolutePath());
                            Main.logger.info("Updated fabric mod " + id);
                            Main.INSTANCE.getSettings().saveString(id + "_fabric_version", fabricModVersion);
                            Main.INSTANCE.getSettings().saveConfig();
                        }
                        updateFabricMod = false;
                    }
                } else {
                    String modSHA = getHash(modFile);
                    if (!modSHA.equalsIgnoreCase(sha1)) {
                        Main.logger.warn("SHA-1 not matching, found " + modSHA.toLowerCase() + " but expected " + sha1.toLowerCase() + ", reinstalling...");
                        Web.downloadMod(link, update.getAbsolutePath());
                        updated = true;
                    } else {
                        Main.logger.info("SHA-1 of mod " + id + " matching");
                    }
                }
            } catch (Exception ex) {
                Main.logger.error("Failed to verify/update EMC mod " + id);
                ex.printStackTrace();
            }
        }).start();
    }

    public static String getHash(File file) throws IOException {
        String sha1 = null;
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e1) {
            throw new IOException("Impossible to get SHA-1 digester", e1);
        }
        try (InputStream input = new FileInputStream(file);
             DigestInputStream digestStream = new DigestInputStream(input, digest)) {
            while (digestStream.read() != -1) { }
            MessageDigest msgDigest = digestStream.getMessageDigest();
            sha1 = HashUtils.printHexBinary(msgDigest.digest());
        }
        return sha1;
    }

    public void install(Consumer<String> cb) {
        if (isInstalled()) {
            Main.logger.info("Mod " + id + " already installed, skipping...");
            return;
        }
        new Thread(() -> {
            if (fabric != null) {
                if (fabricMod != null) {
                    Main.logger.info("Detected fabric mod, installing using fabric method...");
                    try {
                        if (fabricModDelete.exists() && fabricMod.exists()) {
                            fabricModDelete.delete();
                        } else {
                            Main.status = "Installing " + id;
                            installDepends();
                            Web.downloadMod(link, fabricMod.getAbsolutePath());
                        }
                        Main.status = "Installed " + id + ", please restart Minecraft";
                        Main.INSTANCE.getSettings().saveString(id + "_fabric_version", fabricModVersion);
                        Main.INSTANCE.getSettings().saveConfig();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                    Main.status = id + " is not supported on " + IMinecraft.getMinecraftVersion();
                }
            } else {
                if (deleted.exists() && modFile.exists()) {
                    Main.logger.info("Mod already installed but has deletion request, removing request...");
                    if (!deleted.delete()) Main.logger.error("Failed to delete " + deleted.getAbsolutePath());
                    if (!Bootstrap.getMods().containsKey(id) && mod != null) {
                        Bootstrap.getMods().put(id, mod);
                        mod = null;
                    }
                } else {
                    try {
                        Main.logger.info("Installing EMC mod " + id + "...");
                        installDepends();
                        Web.downloadMod(link, modFile.getAbsolutePath());
                        if (modFile.exists()) {
                            loadMod(modFile);
                        }
                    } catch (Exception ex) {
                        Main.logger.error("Failed to install EMC mod " + id);
                        ex.printStackTrace();
                    }
                }
            }
            cb.accept("Installed mod");
        }).start();
    }

    private void loadMod(File file) {
        try {
            JarURLConnection connection = (JarURLConnection) new URL("jar", "", "file:" + file.getAbsolutePath() + "!/client.json").openConnection();
            // Create classLoader
            URLClassLoader classLoader = URLClassLoader.newInstance(
                    new URL[]{new URL("jar", "", "file:" + file.getAbsolutePath() + "!/")},
                    Bootstrap.class.getClassLoader());
            // Open and read json file
            BufferedReader buffer = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            JsonObject json = new Gson().fromJson(buffer.lines().collect(Collectors.joining("\n")), JsonObject.class);
            buffer.close();

            // Load main class from classLoader
            EMCMod instance = (EMCMod) classLoader.loadClass(json.get("main").getAsString()).newInstance();
            // Set instance classLoader
            instance.classLoader = classLoader;

            // Check compatibility
            String[] minVersion = (json.has("minVersion") ? json.get("minVersion").getAsString() : String.format("%s.%s", FrameworkConstants.VERSION, FrameworkConstants.PATCH)).split("\\.");
            if (Double.parseDouble(String.format("%s.%s", minVersion[0], minVersion[1])) >= FrameworkConstants.VERSION && Integer.parseInt(minVersion[2]) > FrameworkConstants.PATCH) {
                Bootstrap.logger.warn("Will not load {}, unsupported EMC version", file.getName());
                return;
            } else if (!json.has("scheme") || json.get("scheme").getAsInt() < FrameworkConstants.SCHEME) {
                Bootstrap.logger.warn("Will not load unsupported mod {}, unsupported scheme", file.getName());
                return;
            }
            // Ensure only one instance is loaded
            if (Bootstrap.getMods().containsKey(json.get("name").getAsString())) {
                return;
            }
            Bootstrap.logger.debug("Loading {} v{} by {}", json.get("name").getAsString(), json.get("version").getAsString(), json.get("author").getAsString());
            Bootstrap.getMods().put(json.get("name").getAsString(), instance);
            Bootstrap.getMods().get(json.get("name").getAsString()).init(json);
            Bootstrap.logger.info("Loaded {}", json.get("name").getAsString());
            
        } catch (Exception ex) {
            Main.logger.error("Failed to bootstrap " + id);
            ex.printStackTrace();
        }
    }

    public void installDepends() {
        if (depends != null && !depends.isEmpty()) {
            Main.logger.info("Found dependencies for mod " + id + ", installing...");
            for (String dependency : depends) {
                Mod dep = API.getMod(dependency);
                if (dep == null) {
                    Main.logger.error("Failed to install dependency: " + dependency + ", not found!");
                } else {
                    if (dep.isInstalled()) {
                        Main.logger.info("Dependency " + dependency + " already installed");
                    } else {
                        Main.logger.info("Installing dependency " + dependency + "...");
                        dep.install((c) -> {
                            Main.logger.info("Installed dependency " + dependency);
                        });
                    }
                }
            }
        }
    }

    public void uninstall() {
        Main.logger.info("Uninstalling EMC mod " + id);
        if (!isInstalled()) {
            Main.logger.error("Mod is not installed, skipping...");
            return;
        }
        new Thread(() -> {
            try {
                if (fabric != null) {
                    FileUtils.writeStringToFile(fabricModDelete, "Delete mod", "UTF-8");
                    Main.logger.info("Uninstalled fabric mod " + id);
                    Main.status = "Uninstalled " + id;
                } else {
                    FileUtils.writeStringToFile(deleted, "Delete mod", "UTF-8");
                    Main.logger.info("Calling uninstall() in " + id);
                    Bootstrap.getMods().get(id).callMethod("uninstall()", "EMC-Marketplace", null);
                    if (Bootstrap.getMods().containsKey(id)) {
                        Main.logger.info("Removing mod from bootstrap");
                        mod = Bootstrap.getMods().get(id);
                        Bootstrap.getMods().remove(id);
                    }
                }
            } catch (IOException e) {
                Main.logger.error("Failed to uninstall mod " + id);
                e.printStackTrace();
            }
        }).start();
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    public static @interface Exclude {}

}
