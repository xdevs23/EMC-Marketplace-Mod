package emc.marketplace.gui;

import me.deftware.client.framework.wrappers.IMinecraft;
import me.deftware.client.framework.wrappers.gui.IGuiButton;
import me.deftware.client.framework.wrappers.gui.IGuiScreen;

import java.util.List;
import java.util.function.Consumer;

public class ConfirmScreen extends IGuiScreen {

	String title;
	List<String> text;
	Consumer<Boolean> onPressed;

	public ConfirmScreen(IGuiScreen parent, String title, List<String> text, Consumer<Boolean> onPressed) {
		super(parent);
		this.title = title;
		this.text = text;
		this.onPressed = onPressed;
	}

	@Override
	protected void onInitGui() {
		this.clearButtons();
		this.addButton(new IGuiButton(0, this.getIGuiScreenWidth() / 2 - 155, this.getIGuiScreenHeight() / 6 + 96, 150, 20, "Continue") {
			@Override
			public void onButtonClick(double mouseX, double mouseY) {
				IMinecraft.setGuiScreen(parent);
				onPressed.accept(true);
			}
		});
		this.addButton(new IGuiButton(1, this.getIGuiScreenWidth() / 2 - 155 + 160, this.getIGuiScreenHeight() / 6 + 96, 150, 20, "Cancel") {
			@Override
			public void onButtonClick(double mouseX, double mouseY) {
				IMinecraft.setGuiScreen(parent);
				onPressed.accept(false);
			}
		});
	}

	@Override
	protected void onDraw(int i, int i1, float v) {
		this.drawITintBackground(0);
		drawCenteredString(title, this.getIGuiScreenWidth() / 2, 40, 0xFFFFFF);
		int y = 90;
		for (String line : text) {
			drawCenteredString(line, this.getIGuiScreenWidth() / 2, y, 0xFFFFFF);
			y += 9;
		}
	}

	@Override
	protected void onUpdate() {

	}

	@Override
	protected void onKeyPressed(int i, int i1, int i2) {

	}

	@Override
	protected void onMouseReleased(int i, int i1, int i2) {

	}

	@Override
	protected void onMouseClicked(int i, int i1, int i2) {

	}

	@Override
	protected void onGuiResize(int i, int i1) {

	}

}
