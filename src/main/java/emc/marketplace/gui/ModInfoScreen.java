package emc.marketplace.gui;

import emc.marketplace.main.Main;
import emc.marketplace.modinstaller.Mod;
import lombok.Getter;
import me.deftware.client.framework.wrappers.IMinecraft;
import me.deftware.client.framework.wrappers.gui.IGuiButton;
import me.deftware.client.framework.wrappers.gui.IGuiScreen;
import me.deftware.client.framework.wrappers.render.IFontRenderer;
import org.lwjgl.glfw.GLFW;

import java.util.Arrays;

public class ModInfoScreen extends IGuiScreen {

	@Getter
	private Mod mod;
	private Main main;

	ModInfoScreen(Mod mod, Main main, IGuiScreen parent) {
		super(parent);
		this.mod = mod;
		this.main = main;
	}

	@Override
	protected void onInitGui() {
		this.clearButtons();
		this.addButton(new IGuiButton(0, getIGuiScreenWidth() / 2 - 100, getIGuiScreenHeight() - 28, 98, 20,
				mod.isInstalled() ? "Uninstall" : "Install") {
			@Override
			public void onButtonClick(double v, double v1) {
				if (mod.isInstalled()) {
					mod.uninstall();
				} else {
					if (mod.isBeta()) {
						IMinecraft.setGuiScreen(new ConfirmScreen(ModInfoScreen.this, "Warning! Beta software:", Arrays.asList(mod.getName() + " is in beta right now,", "installing it may cause instability and/or render problems.", "Install at your own risk, do not report bugs from this addon."), (status) -> {
							if (status) {
								IMinecraft.setGuiScreen(new InstallScreen(ModInfoScreen.this, mod));
							}
						}));
					} else {
						IMinecraft.setGuiScreen(new InstallScreen(ModInfoScreen.this, mod));
					}
				}
			}
		});
		this.addButton(new IGuiButton(1, getIGuiScreenWidth() / 2 + 2, getIGuiScreenHeight() - 28, 98, 20, "Back") {
			@Override
			public void onButtonClick(double v, double v1) {
				IMinecraft.setGuiScreen(new ModListScreen(main, parent));
			}
		});
	}

	@Override
	protected void onDraw(int mouseX, int mouseY, float partialTicks) {
		this.drawITintBackground(0);
		IFontRenderer.drawCenteredString(mod.getName(), getIGuiScreenWidth() / 2, 8, 16777215);
		IFontRenderer.drawCenteredString("Developed by: " + mod.getAuthor(), getIGuiScreenWidth() / 2, 20, 16777215);
		IFontRenderer.drawCenteredString("Mod description:", getIGuiScreenWidth() / 2, 60, 16777215);

		int y = 70;
		for (String string : mod.getDescription()) {
			IFontRenderer.drawCenteredString(string, getIGuiScreenWidth() / 2, y, 16777215);
			y += IFontRenderer.getFontHeight() + 2;
		}
	}

	@Override
	protected void onUpdate() {

	}

	@Override
	protected void onKeyPressed(int keyCode, int action, int modifiers) {
		if (keyCode == GLFW.GLFW_KEY_ESCAPE) {
			IMinecraft.setGuiScreen(null);
		}
	}

	@Override
	protected void onMouseReleased(int mouseX, int mouseY, int mouseButton) {

	}

	@Override
	protected void onMouseClicked(int mouseX, int mouseY, int mouseButton) {

	}

	@Override
	protected void onGuiResize(int w, int h) {

	}

}
