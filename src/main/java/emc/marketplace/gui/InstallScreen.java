package emc.marketplace.gui;

import emc.marketplace.main.Main;
import emc.marketplace.modinstaller.Mod;
import me.deftware.client.framework.wrappers.IMinecraft;
import me.deftware.client.framework.wrappers.gui.IGuiButton;
import me.deftware.client.framework.wrappers.gui.IGuiScreen;

public class InstallScreen extends IGuiScreen {

	private Mod mod;
	private int counter = 0;

	public InstallScreen(IGuiScreen parent, Mod mod) {
		super(parent);
		this.mod = mod;
		Main.status = "Installing mod...";
		mod.install((status) -> {
			Main.status = "Done, mod has been installed";
		});
	}

	@Override
	protected void onInitGui() {
		this.clearButtons();
		this.addButton(new IGuiButton(0, this.getIGuiScreenWidth() / 2 - 155, this.getIGuiScreenHeight() / 6 + 96, 310, 20, "Back") {
			@Override
			public void onButtonClick(double mouseX, double mouseY) {
				IMinecraft.setGuiScreen(parent);
			}
		});
		getIButtonList().get(0).setEnabled(false);
	}

	@Override
	protected void onDraw(int i, int i1, float v) {
		this.drawITintBackground(0);
		drawCenteredString("Installing " + mod.getName(), this.getIGuiScreenWidth() / 2, 40, 0xFFFFFF);
		int y = 90;
		drawCenteredString("Status:", getIGuiScreenWidth() / 2, y, 0xFFFFFF);
		drawCenteredString(Main.status, getIGuiScreenWidth() / 2, y + 9, 0xFFFFFF);
	}

	@Override
	protected void onUpdate() {
		if (Main.status.toLowerCase().contains("done") || Main.status.toLowerCase().contains("restart")) {
			counter++;
		}
		if (counter >= 30) {
			getIButtonList().get(0).setEnabled(true);
		}
	}

	@Override
	protected void onKeyPressed(int i, int i1, int i2) {

	}

	@Override
	protected void onMouseReleased(int i, int i1, int i2) {

	}

	@Override
	protected void onMouseClicked(int i, int i1, int i2) {

	}

	@Override
	protected void onGuiResize(int i, int i1) {

	}

}
