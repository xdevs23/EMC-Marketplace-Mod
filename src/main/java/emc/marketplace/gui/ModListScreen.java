package emc.marketplace.gui;

import emc.marketplace.main.Main;
import emc.marketplace.modinstaller.API;
import emc.marketplace.modinstaller.Mod;
import me.deftware.client.framework.path.OSUtils;
import me.deftware.client.framework.utils.ChatColor;
import me.deftware.client.framework.utils.render.RenderUtils;
import me.deftware.client.framework.wrappers.IMinecraft;
import me.deftware.client.framework.wrappers.gui.IGuiButton;
import me.deftware.client.framework.wrappers.gui.IGuiScreen;
import me.deftware.client.framework.wrappers.gui.IGuiSlot;
import me.deftware.client.framework.wrappers.item.IItemStack;
import me.deftware.client.framework.wrappers.render.*;
import org.lwjgl.glfw.GLFW;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ModListScreen extends IGuiScreen {

    private ModList modList;
    private Main main;

    public ModListScreen(Main main, IGuiScreen parent) {
        super(parent);
        this.main = main;
    }

    @Override
    protected void onInitGui() {
        Main.status = "";
        modList = new ModList(this);
        this.addEventListener(modList);
        modList.clickElement(-1, false, 0, 0);
        this.clearButtons();
        this.addButton(new IGuiButton(0, getIGuiScreenWidth() / 2 - 100, getIGuiScreenHeight() - 28, 200, 20, "Back") {
            @Override
            public void onButtonClick(double v, double v1) {
                IMinecraft.setGuiScreen(parent);
            }
        });
        this.addButton(
                new IGuiButton(1, getIGuiScreenWidth() / 2 - 100, getIGuiScreenHeight() - 51, 98, 20, "Install") {
                    @Override
                    public void onButtonClick(double v, double v1) {
                        if (API.getCachedMods() != null) {
                            Mod selected = (Mod) API.getCachedMods().values().toArray()[modList.getSelectedSlot()];
                            if (selected.isInstalled()) {
                                selected.uninstall();
                            } else {
                                if (selected.isBeta()) {
                                    IMinecraft.setGuiScreen(new ConfirmScreen(ModListScreen.this, "Warning! Beta software:", Arrays.asList(selected.getName() + " is in beta right now,", "installing it may cause instability and/or render problems.", "Install at your own risk, do not report bugs from this addon."), (status) -> {
                                        if (status) {
                                            IMinecraft.setGuiScreen(new InstallScreen(ModListScreen.this, selected));
                                        }
                                    }));
                                } else {
                                    IMinecraft.setGuiScreen(new InstallScreen(ModListScreen.this, selected));
                                }
                            }
                        }
                    }
                });
        this.addButton(
                new IGuiButton(2, getIGuiScreenWidth() / 2 + 2, getIGuiScreenHeight() - 51, 98, 20, "Details...") {
                    @Override
                    public void onButtonClick(double v, double v1) {
                        if (API.getCachedMods() != null) {
                            // We can assume the selected is not null, since you cannot press any of the
                            // buttons if no mod is selected
                            Mod selected = (Mod) API.getCachedMods().values().toArray()[modList.getSelectedSlot()];
                            // More info
                            IMinecraft.setGuiScreen(new ModInfoScreen(selected, main, parent));
                        }
                    }
                });
        if (API.getCachedMods() != null) {
            API.getCachedMods().values().forEach(m -> {
                if (m.getId().equals("fabric-api") && m.isInstalled()) {
                    this.addButton(
                            new IGuiButton(3, 8, getIGuiScreenHeight() - 28, 98, 20, "Fabric mods") {
                                @Override
                                public void onButtonClick(double v, double v1) {
                                    try {
                                        IGuiScreen.openLink(new File(System.getProperty("EMCDir", OSUtils.getMCDir()) + File.separator).toURI().toString());
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                }
                            });
                }
            });
        }
        getIButtonList().get(1).setEnabled(false);
        getIButtonList().get(2).setEnabled(false);
    }

    @Override
    protected void onDraw(int mouseX, int mouseY, float partialTicks) {
        this.drawIDefaultBackground();
        modList.doDraw(mouseX, mouseY, partialTicks);
        IFontRenderer.drawCenteredString("Addons Marketplace", getIGuiScreenWidth() / 2, 8, 16777215);
        IFontRenderer.drawCenteredString((API.getCachedMods() != null ? API.getCachedMods().size() : "0") + " mods available",
                getIGuiScreenWidth() / 2, 20, 16777215);
        if (API.getCachedMods() == null || modList.getISize() == 0) {
            IFontRenderer.drawCenteredString("Loading mods... ", getIGuiScreenWidth() / 2, 45, 16777215);
        }
        // Status
        if (!Main.status.isEmpty()) {
            int stringWidth = IFontRenderer.getStringWidth(Main.status);
            RenderUtils.drawRect((getIGuiScreenWidth() / 2) - (stringWidth / 2), getIGuiScreenHeight() - 51 - 20, (getIGuiScreenWidth() / 2) + (stringWidth / 2), getIGuiScreenHeight() - 51 - 20 + IFontRenderer.getFontHeight(), Color.BLACK);
            IFontRenderer.drawCenteredString(Main.status, getIGuiScreenWidth() / 2, getIGuiScreenHeight() - 51 - 20, 0xFFFFFF);
        }
        IFontRenderer.drawString("Marketplace v" + Main.version, 2, 2 , 0xFFFFFF);
    }

    @Override
    protected void onUpdate() {
        if (API.getCachedMods() == null) {
            return;
        }
        Mod selected = null;
        if (API.getCachedMods() != null && !(API.getCachedMods().size() == 0) && modList.getSelectedSlot() != -1) {
            selected = (Mod) API.getCachedMods().values().toArray()[modList.getSelectedSlot()];
        }
        if (selected != null) {
            getIButtonList().get(1).setEnabled(true);
            getIButtonList().get(2).setEnabled(true);
        } else {
            getIButtonList().get(1).setEnabled(false);
            getIButtonList().get(2).setEnabled(false);
        }
    }

    @Override
    protected void onKeyPressed(int keyCode, int action, int modifiers) {
        if (keyCode == GLFW.GLFW_KEY_ESCAPE) {
            IMinecraft.setGuiScreen(null);
        }
    }

    @Override
    protected void onMouseReleased(int mouseX, int mouseY, int mouseButton) {

    }

    @Override
    protected void onMouseClicked(int mouseX, int mouseY, int mouseButton) {

    }

    @Override
    protected void onGuiResize(int w, int h) {

    }

    private class ModList extends IGuiSlot {

        ModList(IGuiScreen parent) {
            super(parent.getIGuiScreenWidth(), parent.getIGuiScreenHeight(), 36, parent.getIGuiScreenHeight() - 56, 54);
        }

        @Override
        protected int getISize() {
            return API.getCachedMods() != null ? API.getCachedMods().size() : 0;
        }

        @Override
        protected void drawISlot(int id, int x, int y) {
            if (API.getCachedMods() == null) {
                return;
            }
            Mod mod = (Mod) API.getCachedMods().values().toArray()[id];

            IGlStateManager.enableRescaleNormal();
            IGlStateManager.enableBlend();
            IRenderHelper.enableGUIStandardItemLighting();
            IGlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);

            IItemStack stack = new IItemStack(mod.isInstalled() ? "emerald" : "diamond");

            try {
                IRenderItem.renderItemAndEffectIntoGUI(stack, x + 4, y + 4);
                IRenderItem.renderItemOverlays(stack, x + 4, y + 4);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            IRenderHelper.disableStandardItemLighting();
            IGlStateManager.disableRescaleNormal();
            IGlStateManager.disableBlend();
            IGL11.disableLightning();

            IFontRenderer.drawString(ChatColor.BOLD + mod.getName() + " " + ChatColor.RESET + ChatColor.ITALIC + "By " + mod.getAuthor(), x + 31, y + 3, 10526880);

            // Description
            int maxWidth = 180;
            List<String> description = new ArrayList<>();
            if (!mod.getSummary().contains(" ")) {
                description.add(mod.getSummary());
            } else {
                String current = "";
                for (String word : mod.getSummary().split(" ")) {
                    if (current.isEmpty()) {
                        current = word;
                    } else {
                        if (IFontRenderer.getStringWidth(current + " " + word) > maxWidth) {
                            description.add(current);
                            current = word;
                        } else {
                            current += " " + word;
                        }
                    }
                }
                description.add(current);
            }
            y += 15;
            x += 31;
            for (int i = 0; i < description.size(); i++) {
                if (i < 3) {
                    IFontRenderer.drawString(description.get(i), x, y, 10526880);
                    y += 12;
                }
            }
        }

    }

}
